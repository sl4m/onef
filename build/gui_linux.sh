#!/bin/sh
set -eu

target="$1"

docker run --rm --user 0:0  -v "$PWD":/usr/src/onef -v "$target":/usr/src/onef/target \
    -w /usr/src/onef onef/builder:latest cargo build --release --target-dir ./target -p onef_gui
