use iced::{button, scrollable};

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct TogglableButton {
    pub state: button::State,
    clicked: bool,
}

impl TogglableButton {
    pub fn new() -> Self {
        Self {
            state: button::State::new(),
            clicked: false,
        }
    }

    pub fn clicked(&self) -> bool {
        self.clicked
    }
    pub fn toggled(mut self) -> Self {
        self.clicked = true;
        self
    }

    pub fn disable(&mut self) {
        self.clicked = true;
    }
    pub fn enable(&mut self) {
        self.clicked = false;
    }
}

pub struct InfoCard {
    pub state: scrollable::State,
    visible: bool,
    content: String,
}
#[allow(dead_code)]
impl InfoCard {
    pub fn new() -> Self {
        Self {
            state: scrollable::State::new(),
            visible: false,
            content: String::from(""),
        }
    }
    pub fn set_contents(&mut self, content: &str) {
        self.content = content.to_owned();
    }
    pub fn get_contents(&self) -> String {
        self.content.clone()
    }

    pub fn is_visible(&self) -> bool {
        self.visible
    }

    pub fn flash(&mut self) {
        self.visible = true;
    }
    pub fn hide(&mut self) {
        self.visible = false;
    }
    pub fn toggle(&mut self) {
        self.visible = !self.visible;
    }
}
