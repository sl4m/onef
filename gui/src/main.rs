// Dependencies {{{
use async_std::task;
use copypasta::ClipboardContext;
use copypasta::ClipboardProvider;

use iced::futures::{channel::mpsc, future};
use iced::text_input::{self, TextInput};
use iced::{
    button, executor, scrollable, Alignment, Application, Button, Color, Column, Command,
    Container, Element, Length, Row, Rule, Scrollable, Settings, Text,
};
use iced_aw::{split, style, Badge, Card, Icon, Split, ICON_FONT};
use iced_native::subscription::Subscription;
use onef;
use rfd::{AsyncFileDialog, FileHandle};
use std::time::Duration;

mod messenger;
use messenger::ClientInfo;
use messenger::Command as Msngrcmd;
mod theme;
mod widget_helpers;
use widget_helpers::{InfoCard, TogglableButton};
// }}}

pub fn start() -> iced::Result {
    App::run(Settings::default())
}

// Pages and Page types {{{
struct Pages {
    pages: Vec<Page>,
    current_pos: usize,
}

enum Page {
    Lobby {
        join_btn: TogglableButton,
        create_btn: TogglableButton,
        room_id: text_input::State,
        room_id_value: String,
        room_topic: text_input::State,
        room_topic_value: String,
        info_card: InfoCard,
        cmd_sender: Option<mpsc::Sender<Msngrcmd>>,
        peer_id: String,
    },

    Room {
        msg_input: text_input::State,
        msg_value: String,
        send_btn: button::State,
        share_btn: button::State,
        share_btn_fn: String,
        file_path_view: String,
        file_path_value: Option<FileHandle>,
        scrollable_chat: scrollable::State,
        scrollable_offset: f32,
        messages: Vec<ChatMessage>,
        file_rows: Vec<(TogglableButton, messenger::PubFile)>,
        leave_btn: button::State,
        info_card: InfoCard,
        main_split_pane: split::State,
        cmd_sender: Option<mpsc::Sender<Msngrcmd>>,
        room_connection: Option<messenger::Connection>,
        client_info: Option<ClientInfo>,
    },
}

// }}}

// ( Peer/Username, ActualMessage )
pub use onef::ChatMessage;

#[derive(Debug)]
enum AppMessage {
    Gui(Interaction),
    LeaveRoom(()),
    NewRoomCon((ClientInfo, messenger::Connection, mpsc::Sender<Msngrcmd>)),
    Messenger(messenger::Event),
    FilePicked(Option<FileHandle>),
}

use messenger::PubFile as File;

#[derive(Debug, Clone)]
#[allow(dead_code)]
enum Interaction {
    LobbyJoinPressed,
    LobbyCreatePressed,
    LobbyRoomIdInput(String),
    LobbyRoomTopicInput(String),

    RoomMessageInput(String),
    RoomSendPressed,
    RoomSelectPressed,
    RoomUploadPressed,
    RoomDownloadPressed(File),
    RoomChatBoxScrolled(f32),
    RoomLeavePressed,
    RoomMainSplitResized(u16),
    InfoCardOpened,
    InfoCardClosed,
}

impl Page {
    fn update(&mut self, message: AppMessage) -> Command<AppMessage> {
        // LOBBY UPDATE LOGIC{{{
        match self {
            Page::Lobby {
                room_id_value,
                room_topic_value,
                info_card,
                cmd_sender,
                create_btn,
                join_btn,
                peer_id,
                ..
            } => match message {
                AppMessage::Gui(Interaction::LobbyJoinPressed) => {
                    info!("User entered room id: {}", room_id_value);
                    if room_id_value.is_empty() {
                        info_card.set_contents("Error! Room ID Cannot Be Empty");
                        info_card.flash();
                    } else {
                        info_card.set_contents("Searching The DHT Please Wait....");
                        info_card.flash();
                        if let Some(cmd_sender) = cmd_sender {
                            cmd_sender
                                .try_send(Msngrcmd::JoinRoom(room_id_value.clone()))
                                .expect(
                                    "to
                                    send a join cmd to sub",
                                );
                            join_btn.disable();
                            create_btn.disable();
                        }
                    }
                }
                AppMessage::Gui(Interaction::LobbyCreatePressed) => {
                    info!("User entered room topic: {}", room_topic_value);
                    if room_topic_value.is_empty() {
                        info_card.set_contents("Error! Room Topic Cannot Be Empty");
                        info_card.flash();
                    } else {
                        info_card.set_contents("Creating Room Please Wait....");
                        info_card.flash();

                        if let Some(cmd_sender) = cmd_sender {
                            cmd_sender
                                .try_send(Msngrcmd::CreateRoom(room_topic_value.clone()))
                                .expect("to send create cmd to subscription");
                            create_btn.disable();
                            join_btn.disable();
                        }
                    }
                }
                AppMessage::Gui(Interaction::LobbyRoomIdInput(mut data)) => {
                    data.truncate(18);
                    *room_id_value = data;
                    info_card.hide();
                }
                AppMessage::Gui(Interaction::LobbyRoomTopicInput(mut data)) => {
                    data.truncate(32);
                    *room_topic_value = data;
                    info_card.hide();
                }
                AppMessage::Gui(Interaction::InfoCardClosed)
                | AppMessage::Gui(Interaction::InfoCardOpened) => {
                    info_card.toggle();
                }
                AppMessage::Messenger(event) => {
                    use messenger::Event;
                    match event {
                        Event::JoinedLobby(id, cs) => {
                            *cmd_sender = Some(cs);
                            *peer_id = id;
                            info_card.set_contents("Successfully joined The Network Lobby");
                            info_card.flash();
                            create_btn.enable();
                            join_btn.enable();
                        }
                        Event::JoinedRoom(room_con) => {
                            info_card.set_contents(&format!(
                                "Joining Room with ID {} and topic {}.\n
                                Hint: Room ID copied to your Clipboard!",
                                &room_con.0.id, &room_con.0.topic
                            ));
                            info_card.flash();

                            let mut ctx = ClipboardContext::new().unwrap();
                            ctx.set_contents(room_con.0.id.clone()).unwrap();

                            info!("ROOM ID: {}", room_con.0.id);

                            room_topic_value.clear();
                            room_id_value.clear();
                            create_btn.enable();
                            join_btn.enable();

                            if let Some(cmd_sender) = cmd_sender {
                                let clinfo = ClientInfo::new(peer_id.clone(), room_con.0.clone());
                                return Command::perform(
                                    future::ready((clinfo, room_con, cmd_sender.clone())),
                                    AppMessage::NewRoomCon,
                                );
                            }
                        }
                        Event::FailedRoomJoin(error) => {
                            info_card.set_contents(&format!("Could not Enter Room\n{} ", error));
                            info_card.flash();

                            create_btn.enable();
                            join_btn.enable();
                        }
                        Event::FailedLobbyJoin(error) => {
                            info_card.set_contents(&format!(
                                "Could Join Lobby\n{} \nCheck your config and try again ",
                                error
                            ));
                            info_card.flash();
                            panic!(
                                "Cannot Reach BootNode, check your internet connectivity or
                                config input and try again!"
                            )
                        }
                        _ => {}
                    }
                }
                AppMessage::LeaveRoom(()) => {
                    info_card.set_contents("Successfully Left Room");
                    info_card.flash();

                    create_btn.enable();
                    join_btn.enable();
                }
                _ => {}
            },
            // }}}

            // ROOM UPDATE LOGIC {{{
            Page::Room {
                scrollable_offset,
                main_split_pane,
                msg_value,
                file_path_view,
                file_path_value,
                share_btn_fn,
                info_card,
                messages,
                file_rows,
                cmd_sender,
                room_connection,
                client_info,
                ..
            } => match message {
                AppMessage::Gui(Interaction::RoomMessageInput(mut data)) => {
                    data.truncate(100);
                    *msg_value = data;
                }
                AppMessage::Gui(Interaction::RoomSendPressed) => {
                    if !msg_value.is_empty() {
                        if let Some(messenger::Connection(_, msg_sender)) = room_connection {
                            messages.push(ChatMessage("you".to_owned(), msg_value.clone()));
                            msg_sender
                                .try_send(msg_value.clone())
                                .expect("Chat msg to be sent to subscription");
                            msg_value.clear();
                        }
                    }
                }
                AppMessage::Gui(Interaction::RoomChatBoxScrolled(offset)) => {
                    *scrollable_offset = offset;
                }
                AppMessage::Gui(Interaction::RoomMainSplitResized(position)) => {
                    main_split_pane.set_divider_position(position)
                }
                AppMessage::Gui(Interaction::RoomLeavePressed) => {
                    if let Some(cmd_sender) = cmd_sender {
                        cmd_sender
                            .try_send(Msngrcmd::LeaveRoom)
                            .expect("to send leave cmd to subscription");
                        messages.clear();
                        file_rows.clear();
                    }
                }
                AppMessage::Gui(Interaction::InfoCardClosed)
                | AppMessage::Gui(Interaction::InfoCardOpened) => {
                    info_card.toggle();
                }
                AppMessage::NewRoomCon((clinfo, msg_sender, cmd_seder)) => {
                    *room_connection = Some(msg_sender);
                    *client_info = Some(clinfo);
                    *cmd_sender = Some(cmd_seder);
                }
                AppMessage::Gui(Interaction::RoomSelectPressed) => {
                    let file = AsyncFileDialog::new().set_directory("~").pick_file();
                    return Command::perform(file, AppMessage::FilePicked);
                }
                AppMessage::Gui(Interaction::RoomUploadPressed) => {
                    info!("Uploading {:?}", file_path_value);
                    if let Some(file_path_value) = file_path_value {
                        if let Some(cmd_sender) = cmd_sender {
                            cmd_sender
                                .try_send(Msngrcmd::ProvideFile((
                                    file_path_value.path().to_owned(),
                                    file_path_value.file_name(),
                                )))
                                .expect("to send provide file cmd to subscription");
                        }
                    }
                    *file_path_value = None;
                    *share_btn_fn = String::from("Select File");
                    file_path_view.clear();
                }
                AppMessage::Gui(Interaction::RoomDownloadPressed(file)) => {
                    info!("Downloading {:?}", file);
                    file_rows
                        .iter_mut()
                        .find(|fr| fr.1 == file)
                        .unwrap()
                        .0
                        .disable();

                    if let Some(cmd_sender) = cmd_sender {
                        cmd_sender
                            .try_send(Msngrcmd::GetFile(file))
                            .expect("to send provide file cmd to subscription");
                    }
                }
                AppMessage::FilePicked(file_picked) => {
                    if let Some(file) = file_picked {
                        *file_path_view = file.path().to_string_lossy().to_string();
                        *share_btn_fn = String::from("Upload File");
                        *file_path_value = Some(file);
                    } else {
                        *file_path_view =
                            String::from("You did not select a file! please try again");
                        *share_btn_fn = String::from("Select File");
                        *file_path_value = None;
                    }
                }
                AppMessage::Messenger(event) => {
                    use messenger::Event;
                    match event {
                        Event::LeftRoom => {
                            info!("Leaving Room...");
                            return Command::perform(
                                task::sleep(Duration::from_secs(1)),
                                AppMessage::LeaveRoom,
                            );
                        }
                        Event::NewMessage(msg) => {
                            messages.push(msg);
                        }
                        Event::NewFile(pubfile) => {
                            let mut clkd_btn = TogglableButton::new();
                            clkd_btn.enable();

                            if !file_rows.iter().map(|fr| &fr.1).any(|pf| pf == &pubfile) {
                                file_rows.push((TogglableButton::new(), pubfile));
                            } else {
                                warn!("Already Aware of file {:?}", pubfile);
                            }
                        }
                        Event::FileShared => {
                            info_card.set_contents("Successfully Shared The file!");
                            info_card.flash();
                        }
                        Event::FileDownloaded(pubfile) => {
                            info_card
                                .set_contents(&format!("{} Downloaded Successfully! ", pubfile.0));
                            info_card.flash();
                            file_rows
                                .iter_mut()
                                .find(|fr| fr.1 == pubfile)
                                .unwrap()
                                .0
                                .disable();
                        }
                        Event::FailedDownload(pubfile) => {
                            info_card.set_contents(&format!("{} Download Failed! ", pubfile.0));
                            info_card.flash();
                            file_rows
                                .iter_mut()
                                .find(|fr| fr.1 == pubfile)
                                .unwrap()
                                .0
                                .enable();
                        }
                        Event::FailedSend => {
                            error!("Failed to send msg");
                        }
                        _ => {}
                    }
                }
                _ => {}
            },
        }
        // }}}

        Command::none()
    }

    // View Method {{{
    fn view(&mut self) -> Element<AppMessage> {
        match self {
            // LOBBY ROOM VIEW LOGIC {{{
            Page::Lobby {
                join_btn,
                create_btn,
                room_id,
                room_id_value,
                room_topic,
                room_topic_value,
                info_card,
                peer_id,
                ..
            } => {
                let lobby_title = Text::new("     Welcome To 1nf! ")
                    .size(50)
                    .color(theme::ACTIVE);

                let join_room_column: Column<Interaction> = Column::new()
                    .align_items(Alignment::Center)
                    .padding(20)
                    .spacing(20)
                    .push(Text::new("Join Room"))
                    .push(
                        TextInput::new(
                            room_id,
                            " Room ID...",
                            &room_id_value,
                            Interaction::LobbyRoomIdInput,
                        )
                        //.on_submit(Interaction::LobbyJoinPressed)
                        .width(Length::Units(350))
                        .style(theme::TextInput),
                    )
                    .push(if !join_btn.clicked() {
                        Button::new(&mut join_btn.state, Text::new("Join"))
                            .on_press(Interaction::LobbyJoinPressed)
                            .style(theme::Button)
                    } else {
                        Button::new(&mut join_btn.state, Text::new("Join"))
                    });

                let create_room_column: Column<Interaction> = Column::new()
                    .align_items(Alignment::Center)
                    .padding(20)
                    .spacing(20)
                    .push(Text::new("Create Room"))
                    .push(
                        TextInput::new(
                            room_topic,
                            " Room Topic...",
                            &room_topic_value,
                            Interaction::LobbyRoomTopicInput,
                        )
                        //.on_submit(Interaction::LobbyCreatePressed)
                        .width(Length::Units(350))
                        .style(theme::TextInput),
                    )
                    .push(if !create_btn.clicked() {
                        Button::new(&mut create_btn.state, Text::new("Create"))
                            .on_press(Interaction::LobbyCreatePressed)
                            .style(theme::Button)
                    } else {
                        Button::new(&mut create_btn.state, Text::new("Create"))
                    });

                let choice_row = Row::new()
                    .align_items(Alignment::Center)
                    .padding(20)
                    .spacing(20)
                    .push(join_room_column)
                    .push(Rule::vertical(3))
                    .push(create_room_column);

                let main_content = if info_card.is_visible() {
                    let info_text = Text::new(info_card.get_contents());
                    let card = Card::new(Text::new("Info"), info_text)
                        .on_close(Interaction::InfoCardClosed)
                        .style(style::card::Warning);

                    let scrollable_card = Scrollable::new(&mut info_card.state)
                        .max_width(600)
                        .push(card);
                    Column::new()
                        .align_items(Alignment::Center)
                        .spacing(20)
                        .push(scrollable_card)
                        .push(lobby_title)
                        .push(choice_row)
                } else {
                    Column::new()
                        .align_items(Alignment::Center)
                        .spacing(20)
                        .push(
                            Container::new(
                                Badge::new(Text::new(format!(" Your PeerID: {} \n",peer_id)))
                                .style(style::badge::Dark),
                            )
                            .width(Length::Fill)
                            //.align_x(alignment::Horizontal::Right)
                            //.align_y(alignment::Vertical::Bottom),
                        )
                        .push(lobby_title)
                        .push(choice_row)
                };

                let c: Element<Interaction> = Container::new(main_content)
                    .width(Length::Fill)
                    .height(Length::Fill)
                    .padding(20)
                    .center_x()
                    .center_y()
                    .style(theme::Container)
                    .into();
                c.map(AppMessage::Gui)
            }
            //}}}

            // ROOM VIEW LOGIC {{{
            Page::Room {
                msg_input,
                msg_value,
                send_btn,
                share_btn,
                share_btn_fn,
                messages,
                file_rows,
                scrollable_chat,
                file_path_view,
                leave_btn,
                info_card,
                client_info,
                main_split_pane,
                ..
            } => {
                let mut msgs = String::new();

                for msg in messages {
                    if msg.0.len() >= 44 {
                        msgs.push_str(&format!("{}: {}", &msg.0[44..], msg.1));
                    } else {
                        msgs.push_str(&format!("{}: {}", msg.0, msg.1));
                    }
                    msgs.push_str("\n");
                }

                let chat_box = Scrollable::new(scrollable_chat)
                    .on_scroll(move |offset| Interaction::RoomChatBoxScrolled(offset))
                    .spacing(10)
                    .padding(10)
                    .width(Length::Fill)
                    .height(Length::Fill)
                    .push(Text::new(msgs));

                let chat_input = TextInput::new(
                    msg_input,
                    " your message...",
                    msg_value,
                    Interaction::RoomMessageInput,
                )
                .on_submit(Interaction::RoomSendPressed)
                .size(25)
                .style(theme::TextInput);

                let chat_send = Button::new(send_btn, Text::new("send"))
                    .on_press(Interaction::RoomSendPressed)
                    .style(theme::Button);

                let chat_section = Column::new()
                    .align_items(Alignment::Center)
                    .spacing(20)
                    .push(chat_box)
                    .push(chat_input)
                    .push(chat_send);

                let mut share_btn =
                    Button::new(share_btn, Text::new(share_btn_fn.clone())).style(theme::Button);
                if share_btn_fn.contains("Select") {
                    share_btn = share_btn.on_press(Interaction::RoomSelectPressed);
                } else {
                    share_btn = share_btn.on_press(Interaction::RoomUploadPressed);
                }

                let files_section = Column::new().align_items(Alignment::Center).spacing(20);
                let files_section =
                    file_rows
                        .iter_mut()
                        .fold(files_section, |column, (dbtn, file)| {
                            column.push(
                                Row::new()
                                    .push(
                                        Text::new(&format!("{} ", &file.0))
                                            .color(Color::from_rgb8(240, 0, 55)),
                                    )
                                    .push(Text::new(&format!(
                                        "By Peer {}   \n",
                                        &file.1.to_base58()[44..]
                                    )))
                                    .push({
                                        if !dbtn.clicked() {
                                            Button::new(
                                                &mut dbtn.state,
                                                Text::new(Icon::Download).font(ICON_FONT),
                                            )
                                            .on_press(Interaction::RoomDownloadPressed(File(
                                                file.0.clone(),
                                                file.1.clone(),
                                            )))
                                            .style(theme::Button)
                                        } else {
                                            Button::new(
                                                &mut dbtn.state,
                                                Text::new(Icon::Download).font(ICON_FONT),
                                            )
                                        }
                                    }),
                            )
                        });

                let upload_section = Column::new()
                    .align_items(Alignment::Center)
                    .push(share_btn)
                    .push(Text::new(format!("Selected: {}", file_path_view.clone())));

                let file_section = Column::new()
                    .align_items(Alignment::Center)
                    .push(Text::new("Current Downloadable Files:"))
                    .push(files_section)
                    .push(Rule::horizontal(1))
                    .padding(20)
                    .spacing(20)
                    .push(upload_section);

                let room_content = if info_card.is_visible() {
                    let info_text = Text::new(info_card.get_contents());
                    let card = Card::new(Text::new("Info"), info_text)
                        .on_close(Interaction::InfoCardClosed)
                        .style(style::card::Warning);

                    Column::new()
                        .spacing(10)
                        .align_items(Alignment::Center)
                        .push(
                            Scrollable::new(&mut info_card.state)
                                .max_width(600)
                                .push(card),
                        )
                        .push(
                            Button::new(
                                leave_btn,
                                Text::new("Leave Room").color(Color::from_rgb8(255, 0, 0)),
                            )
                            .on_press(Interaction::RoomLeavePressed)
                            .style(theme::Button),
                        )
                        .push(
                            Split::new(
                                main_split_pane,
                                file_section,
                                chat_section,
                                Interaction::RoomMainSplitResized,
                            )
                            .spacing(1.0)
                            .padding(10.0),
                        )
                } else {
                    //if let Some(clinfo) = client_info {
                    //}

                    let clinfo = client_info.clone().unwrap();
                    let card = Card::new(
                        Text::new("Client Info"),
                        Column::new()
                            .push(Text::new(&format!("[Your PeerID]: {}", clinfo.id)))
                            .push(
                                Text::new(&format!(
                                    "Room ID: {}\nTopic: {}",
                                    clinfo.room.id, clinfo.room.topic
                                ))
                                .color(Color::from_rgb8(240, 0, 55)),
                            ),
                    )
                    .style(style::card::Dark);

                    Column::new()
                        .spacing(10)
                        .align_items(Alignment::End)
                        .push(card)
                        .push(
                            Button::new(
                                leave_btn,
                                Text::new("Leave Room").color(Color::from_rgb8(71, 8, 8)),
                            )
                            .on_press(Interaction::RoomLeavePressed)
                            .style(theme::Button),
                        )
                        .push(
                            Split::new(
                                main_split_pane,
                                file_section,
                                chat_section,
                                Interaction::RoomMainSplitResized,
                            )
                            .spacing(1.0)
                            .padding(10.0),
                        )
                };

                let c: Element<Interaction> = Container::new(room_content)
                    .width(Length::Fill)
                    .height(Length::Fill)
                    .padding(20)
                    .center_x()
                    .center_y()
                    .style(theme::Container)
                    .into();
                c.map(AppMessage::Gui)
            } // }}}
        }
    }
    // }}}
}

impl Pages {
    // {{{

    fn new() -> Self {
        Pages {
            pages: vec![
                Page::Lobby {
                    join_btn: TogglableButton::new().toggled(),
                    create_btn: TogglableButton::new().toggled(),
                    room_id: text_input::State::new(),
                    room_id_value: String::new(),
                    room_topic: text_input::State::new(),
                    room_topic_value: String::new(),
                    info_card: InfoCard::new(),
                    cmd_sender: None,
                    peer_id: String::from(""),
                },
                Page::Room {
                    msg_input: text_input::State::new(),
                    msg_value: String::new(),
                    scrollable_chat: scrollable::State::new(),
                    scrollable_offset: 0.0,
                    send_btn: button::State::new(),
                    share_btn: button::State::new(),
                    share_btn_fn: String::from("Select file"),
                    file_path_view: String::from(""),
                    file_path_value: None,
                    leave_btn: button::State::new(),
                    info_card: InfoCard::new(),
                    messages: vec![],
                    file_rows: vec![],
                    main_split_pane: split::State::new(None, split::Axis::Vertical),
                    cmd_sender: None,
                    room_connection: None,
                    client_info: None,
                },
            ],
            current_pos: 0,
        }
    }

    fn next_page(&mut self) {
        self.current_pos += 1;
    }

    fn prev_page(&mut self) {
        self.current_pos -= 1;
    }

    fn title(&self) -> String {
        match &self.pages[self.current_pos] {
            Page::Lobby { .. } => String::from("1nf"),
            Page::Room {
                client_info: Some(client_info),
                ..
            } => {
                format!("Inside room {}", client_info.room.id)
            }
            Page::Room {
                client_info: None, ..
            } => {
                format!("Inside room")
            }
        }
    }

    fn update(&mut self, msg: AppMessage) -> Command<AppMessage> {
        match msg {
            AppMessage::NewRoomCon(_) => self.next_page(),
            AppMessage::LeaveRoom(_) => {
                self.prev_page();
            }
            _ => {}
        }
        self.pages[self.current_pos].update(msg)
    }

    fn view(&mut self) -> Element<AppMessage> {
        self.pages[self.current_pos].view()
    }
    // }}}
}

// Impl App {{{

struct App {
    pages: Pages,
}

pub use onef::ClientStatus;
impl Application for App {
    type Executor = executor::Default;
    type Message = AppMessage;
    type Flags = ();

    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        (
            App {
                pages: Pages::new(),
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        self.pages.title()
    }

    fn subscription(&self) -> Subscription<AppMessage> {
        messenger::start().map(AppMessage::Messenger)
    }

    fn update(&mut self, message: AppMessage) -> Command<Self::Message> {
        self.pages.update(message)
    }

    fn view(&mut self) -> Element<AppMessage> {
        self.pages.view()
    }
}

use env_logger::fmt::WriteStyle;
use env_logger::Builder;
use log::LevelFilter;
use log::{error, info, warn};
use std::io::Write;

pub fn main() {
    // Handle the logging
    let mut builder = Builder::from_default_env();
    builder
        .format(|buf, record| {
            let ts = buf.timestamp();
            let level_style = buf.default_level_style(record.level().clone());

            writeln!(
                buf,
                "[{}| ONEF_GUI {}] - {}",
                ts,
                level_style.value(record.level()),
                record.args()
            )
        })
        .filter(Some("iced"), LevelFilter::Error)
        .filter(Some("wgpu"), LevelFilter::Error)
        .filter(Some("libp2p"), LevelFilter::Error)
        .write_style(WriteStyle::Always)
        .init();
    info!("starting Logger");

    // Start the GUI
    start().expect("Error couldnot start!");
}

// vim:foldmethod=marker
