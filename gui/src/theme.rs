use iced::{button, container, text_input, Color};

const BACKGROUND: Color = Color::from_rgb(
    0x26 as f32 / 255.0,
    0x1D as f32 / 255.0,
    0x3B as f32 / 255.0,
);

const TEXT: Color = Color::WHITE;

pub const ACTIVE: Color = Color::from_rgb(
    0x77 as f32 / 255.0,
    0x00 as f32 / 255.0,
    0x4D as f32 / 255.0,
);

const HOVERED: Color = Color::from_rgb(
    0x84 as f32 / 255.0,
    0x48 as f32 / 255.0,
    0x6F as f32 / 255.0,
);
pub struct Container;

impl container::StyleSheet for Container {
    fn style(&self) -> container::Style {
        container::Style {
            text_color: TEXT.into(),
            background: BACKGROUND.into(),
            ..container::Style::default()
        }
    }
}

pub struct Button;

impl button::StyleSheet for Button {
    fn active(&self) -> button::Style {
        button::Style {
            background: ACTIVE.into(),
            border_radius: 3.0,
            text_color: Color::WHITE,
            ..button::Style::default()
        }
    }

    fn hovered(&self) -> button::Style {
        button::Style {
            background: HOVERED.into(),
            text_color: Color::WHITE,
            ..self.active()
        }
    }

    fn pressed(&self) -> button::Style {
        button::Style {
            border_width: 1.0,
            border_color: Color::WHITE,
            ..self.hovered()
        }
    }
}

pub struct TextInput;

impl text_input::StyleSheet for TextInput {
    fn active(&self) -> text_input::Style {
        text_input::Style {
            background: Color::BLACK.into(),
            border_radius: 2.0,
            border_width: 0.0,
            border_color: Color::TRANSPARENT,
        }
    }

    fn focused(&self) -> text_input::Style {
        text_input::Style {
            border_width: 1.0,
            border_color: HOVERED,
            ..self.active()
        }
    }

    fn hovered(&self) -> text_input::Style {
        text_input::Style {
            border_width: 1.0,
            border_color: Color { a: 0.3, ..HOVERED },
            ..self.focused()
        }
    }

    fn placeholder_color(&self) -> Color {
        Color::from_rgb(0.4, 0.4, 0.4)
    }

    fn value_color(&self) -> Color {
        Color::WHITE
    }

    fn selection_color(&self) -> Color {
        ACTIVE
    }
}
