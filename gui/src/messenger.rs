use async_std::task;
use iced::futures::channel::mpsc;
use iced::futures::StreamExt;
use iced_native::subscription::{self, Subscription};
use onef::Cfg;
use onef::ClientError;
use onef::PeerId;
use std::path::PathBuf;
use onef::{room::Room, ChatMessage, Client};

#[derive(Debug)]
enum UserState {
    Disconnected,
    InLobby(Client,mpsc::Receiver<Command>),
    InRoom(Client,Room,mpsc::UnboundedReceiver<ChatMessage>,mpsc::Receiver<String>,mpsc::Receiver<Command>),
}

#[derive(Debug,Clone,Eq,PartialEq,Hash)]
pub struct PubFile(pub String,pub PeerId);
// (FileName, PeerId)

#[derive(Debug, Clone)]
#[allow(dead_code)]
pub enum Event {
    JoinedRoom(Connection),
    NewMessage(ChatMessage),
    FileDownloaded(PubFile),
    FileShared,
    NewFile(PubFile),
    JoinedLobby(String,mpsc::Sender<Command>),
    FailedLobbyJoin(ClientError),
    LeftRoom,
    FailedRoomJoin(ClientError),
    FailedSend,
    FailedDownload(PubFile),
}

// A Way for the GUI client to send chat msgs and for us to receive and forward to our onef::Client
// instance
// Connection(Room,MsgSender)
#[derive(Debug, Clone)]
pub struct Connection(pub Room,pub mpsc::Sender<String>);

#[derive(Debug, Clone)]
#[allow(dead_code)]
pub enum Command {
    CreateRoom(String),
    JoinRoom(String),
    LeaveRoom,
    ProvideFile((PathBuf,String)),
    GetFile(PubFile),
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct ClientInfo {
    pub id: String,
    pub room: Room
}
#[allow(dead_code)]
impl ClientInfo {
    pub fn new(id: String, room: Room ) -> Self {
        Self {
            id,
            room,
        }
    }
}

pub fn start() -> Subscription<Event> {
    struct Starter;

    subscription::unfold(std::any::TypeId::of::<Starter>(),UserState::Disconnected,
    |state| async move {
        match state {
            UserState::Disconnected => {
                let cfg= Cfg::new().unwrap_or_default();
                // Initilize a onef::client
                // run the event loop on a task
                // and give the client instance to the next State which is InLobby
                let (mut client, event_loop): (onef::Client, onef::EventLoop) =
                                               task::block_on(onef::new(cfg.clone())).expect("client
                                                   and event loop to be init");
                task::spawn(event_loop.run());
                match task::block_on(client.join_lobby(cfg.port)) {
                    Ok(_)=> {
                            let (cmd_sender,cmd_reciver) = mpsc::channel::<Command>(0);
                            (Some(Event::JoinedLobby(client.id.clone().to_base58(),cmd_sender)),UserState::InLobby(client,cmd_reciver))
                    }
                    Err(e)=> {
                        (Some(Event::FailedLobbyJoin(e)),UserState::Disconnected)
                    }
                }

            },
            UserState::InLobby(mut client,mut cmd_reciver) => {
                // Wait for a command from the gui either to create a room or join one
                // Use the Client given to us by the previous state to connect/create room
                match cmd_reciver.select_next_some().await {
                    Command::CreateRoom(topic) => {
                        match client.create_room(topic.clone()).await {
                            Ok((room,msg_rcvr )) => {
                                // Set our state to be InRoom with a room instance, a mpsc
                                // unbounded reciver (given back to us when calling
                                // create_room/join_room), and a mpsc sender for us to receive chat
                                // msgs from the gui
                                let (local_send_tx,local_send_rx) = mpsc::channel::<String>(1);
                                (Some(Event::JoinedRoom(Connection(room.clone(), local_send_tx))),UserState::InRoom(client,room.clone(),msg_rcvr,local_send_rx,cmd_reciver))
                            }
                            Err(e) => {
                                (Some(Event::FailedRoomJoin(e)),UserState::InLobby(client,cmd_reciver))
                            }
                        }
                    }
                    Command::JoinRoom(id) => {
                        match client.join_room(id.clone()).await {
                            Ok((room,msg_rcvr)) => {
                                let (local_send_tx,local_send_rx) = mpsc::channel::<String>(1);
                                (Some(Event::JoinedRoom(Connection(room.clone(),local_send_tx))),UserState::InRoom(client,room.clone(),msg_rcvr,local_send_rx,cmd_reciver))
                            }
                            Err(e) => {
                                (Some(Event::FailedRoomJoin(e)),UserState::InLobby(client,cmd_reciver))
                            }
                        }

                    }
                    Command::LeaveRoom => {
                        match client.leave_room().await {
                            Ok(_) => (Some(Event::LeftRoom),UserState::InLobby(client,cmd_reciver)),
                            Err(_) => (None,UserState::InLobby(client,cmd_reciver))
                        }
                    }
                    _ => {
                            (None,UserState::InLobby(client,cmd_reciver))

                    }

                }
            }
            UserState::InRoom(mut client,room,mut msg_rcvr,mut local_send_rx,mut cmd_reciver) => {
                // poll both chat msg receiver and msgs to send receiver and act accordingly
                // When a new msg comes in send a NewMessage Event
                // when the gui sends us a `String` msg we use our client instance to actually send
                // it
                // after both operations we set our UserState to be InRoom
                // The above allows the matching to repeat again mimicking a loop behaviour
                // (staying in a room)
                futures::select! {
                    msg_to_send = local_send_rx.select_next_some() => {
                        match client.send_message(msg_to_send).await {
                            Ok(_) => (None, UserState::InRoom(client,room,msg_rcvr,local_send_rx,cmd_reciver)),
                            Err(_) => (Some(Event::FailedSend), UserState::InRoom(client,room,msg_rcvr,local_send_rx,cmd_reciver)),
                        }
                    }
                    new_msg = msg_rcvr.select_next_some() => {
                        if new_msg.1.starts_with("/nfile/") {
                            let new_file = std::path::PathBuf::from(new_msg.1);
                            let mut new_file = new_file.iter();
                            new_file.next().expect("[MSNGR] to pop nfile");
                            new_file.next().expect("[MSNGR] to pop nfile");
                            let peer = new_file.next().expect("[MSNGR] to peer id").to_str().expect("[MSNGR] Invalid PeerId");
                            let peer: PeerId =
                                PeerId::try_from_multiaddr(&format!("/p2p/{}",peer).parse()
                                .expect("[MSNGR] Invalid PeerId"))
                                .expect("[MSNGR] Invalid PeerId");
                            let fname= new_file.next().expect("[MSNGR] to get file name");
                            let new_file = PubFile(fname.to_string_lossy().into(),peer);

                            (Some(Event::NewFile(new_file)), UserState::InRoom(client,room,msg_rcvr,local_send_rx,cmd_reciver))
                        } else {
                            (Some(Event::NewMessage(new_msg)), UserState::InRoom(client,room,msg_rcvr,local_send_rx,cmd_reciver))
                        }

                    }
                    cmd = cmd_reciver.select_next_some() => {
                    match cmd {
                        Command::LeaveRoom => {
                            match client.leave_room().await {
                                Ok(_) => (Some(Event::LeftRoom),UserState::InLobby(client,cmd_reciver)),
                                Err(_) => {
                                    (None, UserState::InRoom(client,room,msg_rcvr,local_send_rx,cmd_reciver))
                                }
                            }
                        },
                        Command::ProvideFile((path,name)) => {
                            client.provide_file(path,name).await.unwrap();
                                    (Some(Event::FileShared), UserState::InRoom(client,room,msg_rcvr,local_send_rx,cmd_reciver))

                        },
                        Command::GetFile(pubfile) => {
                            match client.request_file(pubfile.1.clone(), pubfile.0.clone()).await {
                                Ok(_) =>  {
                                    (Some(Event::FileDownloaded(pubfile)), UserState::InRoom(client,room,msg_rcvr,local_send_rx,cmd_reciver))
                                }
                                Err(_) => {
                                    (Some(Event::FailedDownload(pubfile)), UserState::InRoom(client,room,msg_rcvr,local_send_rx,cmd_reciver))
                                }
                            }

                        },
                            _=> { (None, UserState::InRoom(client,room,msg_rcvr,local_send_rx,cmd_reciver))}
                        }

                    }

                }

            }
        }
    }
    )
}
