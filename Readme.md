# Onef  (WIP)

Library with a graphical program for file sharing and messaging based on a room concept.
    - Uses [rust-Libp2p's](https://github.com/libp2p/rust-libp2p)
        - Kadmelia DHT to publish/Retrieve Room Records across connected peers
        - GossipSub for sending and receiving messages
        - Circuit Relay V2 for connecting private (NATed) peers
        - Dcutr to help traverse NAT
    - a Graphical interface using [Iced](https://github.com/iced-rs/iced)


# TODOs and Current Issues

- NAT traversal isn't always successful
- Need to stream files into and out of disk instead of reading to mem
- Crashes when sending large files `TrySendError gui:267:34`
- Get Rid of Ping Protocol and use GossipSub keep alive feature instead
