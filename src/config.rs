use config::{Config, ConfigError, File};
use dirs;
use libp2p::multiaddr::Multiaddr;
use std::{env, path::PathBuf};

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct Cfg {
    pub port: u16,
    pub dl_dir: PathBuf,
    pub boot_node: Multiaddr,
}
impl Default for Cfg {
    fn default() -> Self {
        //Saved to "~/onef_storage" incase no dl_dir was provided
        let mut dl_path = dirs::home_dir().unwrap_or(".".into());
        dl_path.push("onef_storage");
        Cfg {
            port: 0,
            dl_dir: dl_path,
            boot_node:
            "/ip4/139.177.181.45/tcp/44550/p2p/12D3KooWGAJLc57wg4ZFpQkwLvMidJkMnjthjJSUNH4CKwvZnybh".parse().expect("to
            never fail"),
        }
    }
}

impl Cfg {
    pub fn new() -> Result<Self, ConfigError> {
        let cfg_path = env::var("CFG_PATH").unwrap_or_else(|_| ".".into());
        let cfg = Config::builder()
            .add_source(File::with_name(&format!("{}/config", cfg_path)))
            .set_default("port", 0)
            .unwrap()
            .set_default("dl_dir", {
                let mut dl_path = dirs::home_dir().unwrap_or(".".into());
                dl_path.push("onef_storage");
                format!("{:?}", dl_path)
            })
            .unwrap()
            .set_default("boot_node","/ip4/139.177.181.45/tcp/44550/p2p/12D3KooWGAJLc57wg4ZFpQkwLvMidJkMnjthjJSUNH4CKwvZnybh")
            .unwrap()
            .build()?;

        cfg.try_deserialize()
    }
}
