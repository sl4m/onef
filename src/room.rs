use base58::ToBase58;
use rand::rngs::StdRng;
use rand::{RngCore, SeedableRng};

#[derive(Debug, Clone)]
pub struct Room {
    pub id: String,
    pub topic: String,
    // private: bool,
    // can i do this ^? maybe pnet?
    // pub/sub spec does mentions that there should be EncMode field?
}

impl Room {
    pub fn new(topic: String) -> Self {
        Room {
            id: generate_roomid(),
            topic,
        }
    }
}

pub fn generate_roomid() -> String {
    let mut random_string_bytes: Vec<u8> = vec![0; 12];
    let mut stdrng = StdRng::from_entropy();

    stdrng.try_fill_bytes(&mut random_string_bytes).unwrap();

    random_string_bytes.to_base58()
}
