// Dpendencies {{{
pub mod room;
pub use room::Room;

use futures::channel::mpsc;
use futures::channel::oneshot;
use futures::executor::block_on;
use futures::prelude::*;

use crate::gossipsub::error::GossipsubHandlerError;
use libp2p::core::either::EitherError;
use libp2p::swarm::ConnectionHandlerUpgrErr;

use libp2p::core::upgrade;
use libp2p::dcutr::{self, InboundUpgradeError, OutboundUpgradeError};

pub use autonat::NatStatus;
use libp2p::gossipsub::{
    self, Gossipsub, GossipsubConfigBuilder, GossipsubEvent, IdentTopic, MessageAuthenticity,
    MessageId, ValidationMode,
};
use libp2p::swarm::{
    self,
    //dial_opts::{DialOpts, PeerCondition},
    NetworkBehaviour,
    SwarmEvent,
};

use libp2p::ping::{self, Ping, PingConfig, PingEvent};
pub use libp2p::PeerId;
use libp2p::Transport;
use libp2p::{
    autonat,
    dns::DnsConfig,
    identify::{self, Identify, IdentifyConfig, IdentifyEvent},
    identity,
    kad::{
        self, record::store::MemoryStore, record::Key, Kademlia, KademliaConfig, KademliaEvent,
        QueryId, QueryResult, Quorum, Record,
    },
    multiaddr::{Multiaddr, Protocol},
    noise,
    tcp::TcpConfig,
    NetworkBehaviour, Swarm,
};

use libp2p::core::transport::OrTransport;
use libp2p::core::upgrade::{read_length_prefixed, write_length_prefixed, ProtocolName};
use libp2p::relay::v2::client as RelayClient;
use libp2p::relay::v2::{InboundStopFatalUpgradeError, OutboundHopFatalUpgradeError};
use libp2p::request_response::{
    ProtocolSupport, RequestId, RequestResponse, RequestResponseCodec, RequestResponseEvent,
    RequestResponseMessage,
};

use void::Void;

use log::{debug, error, info, trace, warn};

use async_std::fs;
use async_std::io;

use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::path::PathBuf;
use std::time::Duration;
mod config;
pub use crate::config::Cfg;
// }}}

// Run and get a client back {{{
pub async fn new<'n>(cfg: Cfg) -> Result<(Client, EventLoop), std::io::Error> {
    /* TODO: Have a Export/Import key option! in the config*/

    let local_keypair = identity::Keypair::generate_ed25519();
    let local_peer_id = PeerId::from(local_keypair.public());

    let store = MemoryStore::new(local_peer_id);

    let mut kad_cfg = KademliaConfig::default();
    kad_cfg.set_query_timeout(Duration::from_secs(10));

    //let mut kad = Kademlia::new(local_peer_id, store);
    let mut kad = Kademlia::with_config(local_peer_id, store, kad_cfg);

    kad.add_address(
        &PeerId::try_from_multiaddr(&cfg.boot_node).unwrap(),
        cfg.boot_node.clone(),
    );

    let ident = Identify::new(IdentifyConfig::new(
        "/onef/0.1.0".into(),
        local_keypair.public().clone(),
    ));

    let mut auto_nat = autonat::Behaviour::new(
        local_peer_id,
        autonat::Config {
            retry_interval: Duration::from_secs(5),
            refresh_interval: Duration::from_secs(15 * 60),
            boot_delay: Duration::from_secs(5),
            throttle_server_period: Duration::ZERO,
            ..Default::default()
        },
    );

    auto_nat.add_server(
        PeerId::try_from_multiaddr(&cfg.boot_node).unwrap(),
        Some(cfg.boot_node.clone()),
    );

    let gossipsub_cfg = GossipsubConfigBuilder::default()
        .heartbeat_interval(Duration::from_secs(10))
        .validation_mode(ValidationMode::Strict) // Strict Msg validation aka signing
        .build()
        .expect("Valid config");

    let gossipsub: Gossipsub = Gossipsub::new(
        MessageAuthenticity::Signed(local_keypair.clone()),
        gossipsub_cfg,
    )
    .expect("Correct Configuration");

    // Transport and behaviour

    let (relay_transport, relay_client_behaviour) =
        RelayClient::Client::new_transport_and_behaviour(local_peer_id);

    let noise_keys = noise::Keypair::<noise::X25519Spec>::new()
        .into_authentic(&local_keypair)
        .expect("Signing libp2p-noise static DH keypair failed.");

    let base_transport = TcpConfig::new().nodelay(true).port_reuse(true);

    let relay_or_base_transport = OrTransport::new(relay_transport, base_transport);

    let transport = block_on(DnsConfig::system(relay_or_base_transport))
        .unwrap()
        .upgrade(upgrade::Version::V1)
        .authenticate(noise::NoiseConfig::xx(noise_keys).into_authenticated())
        .multiplex(libp2p::yamux::YamuxConfig::default())
        .boxed();

    let dcutr = dcutr::behaviour::Behaviour::new();

    let request_response = RequestResponse::new(
        FileExchangeCodec(),
        std::iter::once((FileExchangeProtocol(), ProtocolSupport::Full)),
        Default::default(),
    );

    let ping = Ping::new(PingConfig::new().with_keep_alive(true));

    let swarm = swarm::SwarmBuilder::new(
        transport,
        ComposedBehaviour {
            kademlia: kad,
            identity: ident,
            gossipsub,
            auto_nat,
            request_response,
            dcutr,
            relay_client: relay_client_behaviour,
            ping,
        },
        local_peer_id,
    )
    .build();

    let (command_sender, command_receiver) = mpsc::channel(0);

    let client = Client {
        id: local_peer_id,
        status: ClientStatus::NotConnected,
        command_sender,
    };

    let eventloop = EventLoop {
        swarm,
        command_receiver,
        pending_room_joins: HashMap::new(),
        pending_room_creates: HashMap::new(),
        pending_file_requests: HashMap::new(),
        pending_dials: HashMap::new(),
        shared_files: HashMap::new(),
        gossip_sender: None,
        cfg,
    };
    Ok((client, eventloop))
}
//}}}}

// Command Enum {{{
#[derive(Debug)]
enum Command {
    JoinLobby {
        addr: Multiaddr,
        sender: oneshot::Sender<Result<(), ClientError>>,
    },
    FindRoom {
        room_id: String,
        sender: oneshot::Sender<Result<(PeerId, String), ClientError>>,
    },
    JoinRoom {
        metadata: (PeerId, String),
        sender: oneshot::Sender<Result<mpsc::UnboundedReceiver<ChatMessage>, ClientError>>,
    },
    DialPublisher {
        publisher: PeerId,
        sender: oneshot::Sender<Result<(), ClientError>>,
    },
    LeaveRoom {
        room: Room,
        sender: oneshot::Sender<Result<(), ClientError>>,
    },
    CreateRoom {
        room: Room,
        sender: oneshot::Sender<Result<(), ClientError>>,
    },
    SubRoom {
        topic: String,
        messages_sender: mpsc::UnboundedSender<ChatMessage>,
    },
    SendMessage {
        msg: String,
        room_topic: String,
        sender: oneshot::Sender<Result<MessageId, ClientError>>,
    },
    RequestFile {
        fname: String,
        peer: PeerId,
        sender: oneshot::Sender<Result<(), ClientError>>,
    },
    ProvideFile {
        fpath: PathBuf,
    },
}
// }}}

// ClientError {{{
#[derive(Debug, Clone)]
pub enum ClientError {
    FindRoom(String),
    JoinRoom(String),
    LeaveRoom(String),
    CreateRoom(String),
    JoinNetwork(String),
    SendMessage(String),
    RecvMessage(String),
    ChannelError(String),
    NotInARoom,
}
impl Error for ClientError {}

impl fmt::Display for ClientError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ClientError::FindRoom(s)
            | ClientError::JoinRoom(s)
            | ClientError::CreateRoom(s)
            | ClientError::JoinNetwork(s)
            | ClientError::SendMessage(s)
            | ClientError::RecvMessage(s)
            | ClientError::LeaveRoom(s) => {
                write!(f, "[Client Error]: {}", s)
            }
            ClientError::NotInARoom => {
                write!(f, "[Client Error]: Client is not in a room yet")
            }
            ClientError::ChannelError(s) => write!(f, "[Client Data Channel Error]: {}", s),
        }
    }
}

impl From<oneshot::Canceled> for ClientError {
    fn from(e: oneshot::Canceled) -> ClientError {
        ClientError::ChannelError(format!("{}", e))
    }
}
impl From<mpsc::SendError> for ClientError {
    fn from(e: mpsc::SendError) -> ClientError {
        ClientError::ChannelError(format!("{}", e))
    }
}

impl From<gossipsub::error::SubscriptionError> for ClientError {
    fn from(e: gossipsub::error::SubscriptionError) -> ClientError {
        ClientError::ChannelError(format!("{}", e))
    }
}
// }}}

// Event loop handle_event,handle_command,run {{{
pub struct EventLoop {
    swarm: Swarm<ComposedBehaviour>,
    command_receiver: mpsc::Receiver<Command>,
    pending_room_joins: HashMap<QueryId, oneshot::Sender<Result<(PeerId, String), ClientError>>>,
    pending_room_creates: HashMap<QueryId, oneshot::Sender<Result<(), ClientError>>>,
    pending_file_requests: HashMap<RequestId, (String, oneshot::Sender<Result<(), ClientError>>)>,
    pending_dials: HashMap<PeerId, oneshot::Sender<Result<(), ClientError>>>,
    shared_files: HashMap<String, PathBuf>,
    gossip_sender: Option<mpsc::UnboundedSender<ChatMessage>>,
    cfg: Cfg,
}

impl EventLoop {
    pub async fn run(mut self) {
        loop {
            futures::select! {
                    event = self.swarm.next() => self.handle_event(event.expect("Swarm stream to be infinite.")).await  ,
                    command = self.command_receiver.next() => match command {
                        Some(c) => self.handle_command(c).await,
                        None=>  continue,
                    },
            }
        }
    }

    // handle command {{{
    async fn handle_command(&mut self, command: Command) {
        match command {
            Command::JoinLobby { addr, sender } => {
                match self.swarm.listen_on(addr) {
                    Ok(_) => {
                        // Bootstrap Node
                        //self.swarm
                        //.behaviour_mut()
                        //.kademlia
                        //.bootstrap()
                        //.expect("to bootstrap");

                        let boot_node_peerid =
                            PeerId::try_from_multiaddr(&self.cfg.boot_node).unwrap();

                        if self.swarm.is_connected(&boot_node_peerid) {
                            warn!("already connected to bootnode");
                            sender.send(Ok(())).unwrap();
                        } else {
                            warn!("not connected to bootnode, connecting... ");
                            self.pending_dials.insert(boot_node_peerid.clone(), sender);
                        }
                    }
                    Err(e) => {
                        error!("cannot listen {:?}", e);
                        sender
                            .send(Err(ClientError::JoinNetwork(
                                "Couldnt listen on specified addr, check logs".into(),
                            )))
                            .unwrap();
                    }
                }
            }
            Command::FindRoom { room_id, sender } => {
                let query_id = self
                    .swarm
                    .behaviour_mut()
                    .kademlia
                    .get_record(Key::new(&room_id.clone().into_bytes()), Quorum::One);
                self.pending_room_joins.insert(query_id, sender);
            }
            Command::DialPublisher { publisher, sender } => {
                let publisher_addes = self.swarm.behaviour_mut().addresses_of_peer(&publisher);
                debug!(
                    "[GOSSIP] RoomPublisher\n ID {:?}\n Addes {:?}",
                    publisher, publisher_addes
                );
                match self.swarm.is_connected(&publisher) {
                    true => {
                        sender.send(Ok(())).expect("reciver not to be dropped");
                    }
                    false => {
                        //let custom_dial = DialOpts::peer_id(publisher)
                        //.addresses(vec![publisher_relayed.clone()])
                        //.condition(PeerCondition::Disconnected)
                        //.override_role()
                        //.build();

                        let publisher_relayed = self
                            .cfg
                            .boot_node
                            .clone()
                            .with(Protocol::P2pCircuit)
                            .with(Protocol::P2p(publisher.into()));
                        warn!(
                            "Not connected to Publisher Peer Trying Through a RELAY {:?}",
                            publisher_relayed
                        );

                        let _ = self.swarm.dial(publisher_relayed);
                        self.pending_dials.insert(publisher, sender);
                    }
                }
            }
            Command::JoinRoom {
                metadata: (publisher, topic),
                sender,
            } => {
                debug!("Yes Connected To Publisher");
                self.swarm
                    .behaviour_mut()
                    .gossipsub
                    .add_explicit_peer(&publisher);

                let sub_result = self
                    .swarm
                    .behaviour_mut()
                    .gossipsub
                    .subscribe(&IdentTopic::new(topic));

                let _ = match sub_result {
                    // returns yes when great success and no when we already subed
                    Ok(_) => {
                        let (messages_sender, messages_receiver) = mpsc::unbounded::<ChatMessage>();
                        self.gossip_sender = Some(messages_sender);
                        sender.send(Ok(messages_receiver))
                    }
                    Err(_) => sender.send(Err(ClientError::JoinRoom(
                        "Could not Join Room `topic subscription failed`".into(),
                    ))),
                };
            }

            Command::CreateRoom { room, sender } => {
                let query_id = self
                    .swarm
                    .behaviour_mut()
                    .kademlia
                    .put_record(
                        Record::new(
                            Key::new(&room.id.as_bytes()),
                            room.topic.clone().into_bytes(),
                        ),
                        Quorum::One,
                    )
                    .expect("[create_room] Could not put record");

                self.pending_room_creates.insert(query_id, sender);
            }
            Command::SubRoom {
                topic,
                messages_sender,
            } => {
                self.swarm
                    .behaviour_mut()
                    .gossipsub
                    .subscribe(&IdentTopic::new(topic))
                    .expect("[join_room] Could not subscribe to topic");
                self.gossip_sender = Some(messages_sender);
            }
            Command::SendMessage {
                msg,
                room_topic,
                sender,
            } => {
                let msg_id = self
                    .swarm
                    .behaviour_mut()
                    .gossipsub
                    .publish(IdentTopic::new(room_topic), msg);
                if let Ok(msg_id) = msg_id {
                    let _ = sender.send(Ok(msg_id));
                } else {
                    let _ = sender.send(Err(ClientError::SendMessage(
                        "Could not send publish message".to_owned(),
                    )));
                }
            }
            Command::LeaveRoom { room, sender } => {
                self.swarm
                    .behaviour_mut()
                    .kademlia
                    .remove_record(&Key::new(&room.id.as_bytes()));

                self.swarm
                    .behaviour_mut()
                    .gossipsub
                    .unsubscribe(&IdentTopic::new(room.topic.clone()))
                    .expect("couldnot unsubscribe");

                if let Some(gossip_sender) = &self.gossip_sender {
                    gossip_sender.close_channel();
                    self.gossip_sender = None;
                }
                sender.send(Ok(())).unwrap();
            }
            Command::RequestFile {
                fname,
                peer,
                sender,
            } => {
                let req_id = self
                    .swarm
                    .behaviour_mut()
                    .request_response
                    .send_request(&peer, FileRequest(fname.clone()));
                self.pending_file_requests.insert(req_id, (fname, sender));
            }
            Command::ProvideFile { fpath } => {
                let file_name = fpath.file_name().expect("File Name Yok").to_string_lossy();
                self.shared_files.insert(file_name.into_owned(), fpath);
            }
        }
    }
    // }}}

    // handle event {{{
    async fn handle_event(
        &mut self,
        event: SwarmEvent<
            ComposedEvent,
            EitherError<
                EitherError<
                    EitherError<
                        EitherError<
                            EitherError<
                                EitherError<
                                    EitherError<std::io::Error, GossipsubHandlerError>,
                                    std::io::Error,
                                >,
                                ConnectionHandlerUpgrErr<std::io::Error>,
                            >,
                            either::Either<
                                ConnectionHandlerUpgrErr<
                                    EitherError<InboundUpgradeError, OutboundUpgradeError>,
                                >,
                                either::Either<ConnectionHandlerUpgrErr<std::io::Error>, Void>,
                            >,
                        >,
                        ConnectionHandlerUpgrErr<std::io::Error>,
                    >,
                    either::Either<
                        ConnectionHandlerUpgrErr<
                            EitherError<InboundStopFatalUpgradeError, OutboundHopFatalUpgradeError>,
                        >,
                        Void,
                    >,
                >,
                ping::Failure,
            >,
        >,
    ) {
        match event {
            SwarmEvent::NewListenAddr { address, .. } => {
                info!("[Now Listening] on {:?}", address)
            }
            SwarmEvent::Behaviour(event) => match event {
                ComposedEvent::Kadvent(event) => match event {
                    KademliaEvent::OutboundQueryCompleted {
                        result: QueryResult::Bootstrap(Ok(boot_ok)),
                        ..
                    } => {
                        debug!("[Bootstrap Event] {:?}", boot_ok);
                    }
                    KademliaEvent::OutboundQueryCompleted {
                        id,
                        result: QueryResult::GetRecord(Ok(getrecord_ok)),
                        ..
                    } => {
                        let sender = self
                            .pending_room_joins
                            .remove(&id)
                            .expect("To get A sender");

                        if let Some(rec) = getrecord_ok.records.iter().next() {
                            let rec = rec.record.clone();

                            let record_value_utf8 = String::from_utf8(rec.value).unwrap();

                            if let Some(record_publisher) = rec.publisher {
                                sender
                                    .send(Ok((record_publisher, record_value_utf8)))
                                    .expect("Receiver not to be dropped");
                            } else {
                                sender
                                    .send(Err(ClientError::FindRoom(format!(
                                        "Could Not Obtain Publisher's PeerID"
                                    ))))
                                    .expect("reciver not to be dropped");
                            }
                        } else {
                            sender
                                .send(Err(ClientError::FindRoom(format!(
                                    "Could not find any record for QeuryId {:?}",
                                    id
                                ))))
                                .expect("receiver not to be dropped");
                        }
                    }
                    KademliaEvent::OutboundQueryCompleted {
                        id,
                        result: QueryResult::GetRecord(Err(_)),
                        ..
                    } => {
                        let sender = self
                            .pending_room_joins
                            .remove(&id)
                            .expect("Completed Previous Pending Query");
                        sender
                            .send(Err(ClientError::FindRoom("Room Not Found".to_owned())))
                            .expect("Receiver not to be dropped");
                    }
                    KademliaEvent::OutboundQueryCompleted {
                        id,
                        result: QueryResult::PutRecord(Ok(_)),
                        ..
                    } => {
                        let sender = self
                            .pending_room_creates
                            .remove(&id)
                            .expect("Completed Previous Pending Query");
                        sender.send(Ok(())).expect("Receiver not to be dropped");
                    }
                    KademliaEvent::OutboundQueryCompleted {
                        id,
                        result: QueryResult::PutRecord(Err(e)),
                        ..
                    } => {
                        error!("[KADVENT] {}", e);
                        let sender = self
                            .pending_room_creates
                            .remove(&id)
                            .expect("Completed Previous Pending Query");

                        sender
                            .send(Err(ClientError::CreateRoom(
                                "Could Not publish room, check logs".to_owned(),
                            )))
                            .expect("Receiver not to be dropped");
                    }

                    kad_event => {
                        debug!("[KADVENT] {:?}", kad_event)
                    }
                },
                ComposedEvent::Identify(identity_event) => {
                    debug!("[IDENT_EVENT] {:?}", identity_event);
                    if let IdentifyEvent::Received {
                        peer_id,
                        info:
                            identify::IdentifyInfo {
                                listen_addrs,
                                protocols,
                                observed_addr,
                                ..
                            },
                    } = identity_event
                    {
                        info!("New Observed public address: {:?}", observed_addr);
                        if protocols
                            .iter()
                            .any(|p| p.as_bytes() == kad::protocol::DEFAULT_PROTO_NAME)
                        {
                            for addr in listen_addrs {
                                self.swarm
                                    .behaviour_mut()
                                    .kademlia
                                    .add_address(&peer_id, addr);
                            }
                        }
                    }
                }
                ComposedEvent::AutoNat(autonat::Event::StatusChanged { old: _, new }) => {
                    // If We are private connect to a relay
                    if new == autonat::NatStatus::Private || new == autonat::NatStatus::Unknown {
                        let relay_addi: Multiaddr = self.cfg.boot_node.clone();
                        let relay_addi = relay_addi.with(Protocol::P2pCircuit);
                        info!("[NAT STATUS] New NAT STATUS {:?}", new);
                        self.swarm
                            .listen_on(relay_addi)
                            .expect("to listen on bootnode relay");
                    }
                }
                ComposedEvent::AutoNat(_rest_of_events) => {
                    let nat_status = self.swarm.behaviour().auto_nat.nat_status();
                    let nat_status_confidence = self.swarm.behaviour().auto_nat.confidence();
                    debug!(
                        "[NAT STATUS] {:?} with confidence of: {}",
                        nat_status, nat_status_confidence
                    );
                }
                ComposedEvent::Gossipsub(gossip_event) => match gossip_event {
                    GossipsubEvent::Message {
                        propagation_source: peer_id,
                        message_id: id,
                        message,
                    } => {
                        info!(
                            "[GOSSIP] Got message: {} with id: {} from peer: {:?}",
                            String::from_utf8_lossy(&message.data),
                            id,
                            peer_id
                        );
                        let chatmsg = ChatMessage(
                            peer_id.to_base58(),
                            String::from_utf8_lossy(&message.data).to_string(),
                        );
                        if let Some(gsender) = &self.gossip_sender {
                            gsender
                                .unbounded_send(chatmsg)
                                .expect("gossip message to be sent")
                        }
                    }
                    _ => {}
                },

                ComposedEvent::Dctur(con_up) => {
                    info!("[DCUTR] {:?}", con_up);
                    //debug!("[DCUTR] {:?}", con_up);
                }
                ComposedEvent::ReqRes(RequestResponseEvent::Message { peer: _, message }) => {
                    match message {
                        RequestResponseMessage::Request {
                            request_id: _,
                            request,
                            channel,
                        } => match self.shared_files.get(&request.0) {
                            Some(file_path) => {
                                let file_content =
                                    fs::read(file_path).await.expect("to read provided file");
                                self.swarm
                                    .behaviour_mut()
                                    .request_response
                                    .send_response(channel, FileResponse(file_content))
                                    .expect("file to be read and sent");
                            }
                            None => {}
                        },

                        RequestResponseMessage::Response {
                            request_id,
                            response,
                        } => {
                            let did_request = self.pending_file_requests.remove(&request_id);
                            let content = response.0;

                            match did_request {
                                Some(sender) => {
                                    let mut file_path = self.cfg.dl_dir.clone();
                                    file_path.push(sender.0);

                                    match fs::metadata(self.cfg.dl_dir.clone()).await {
                                        Ok(_) => match fs::write(file_path, content).await {
                                            Ok(_) => {
                                                sender.1.send(Ok(())).expect("file downloaded");
                                            }
                                            Err(e) => {
                                                error!("Could not write file {:?}", e)
                                            }
                                        },
                                        Err(_) => {
                                            match fs::create_dir(self.cfg.dl_dir.clone()).await {
                                                Ok(_) => {
                                                    match fs::write(file_path, content).await {
                                                        Ok(_) => {
                                                            sender
                                                                .1
                                                                .send(Ok(()))
                                                                .expect("file downloaded");
                                                        }
                                                        Err(e) => {
                                                            error!("Could not write file {}", e)
                                                        }
                                                    }
                                                }
                                                Err(e) => {
                                                    error!(
                                                        "Could not create download directory {:?}",
                                                        e
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }
                                None => {}
                            }
                        }
                    }
                }
                ComposedEvent::ReqRes(reqres) => {
                    debug!("[REQ_RES] {:?}", reqres);
                }
                ComposedEvent::RelayClientEvent(crly_event) => {
                    debug!("[CLIENT_RELAY] {:?}", crly_event);
                }
                ComposedEvent::Ping(pevent) => {
                    trace!("[PING] {:?}", pevent);
                }
            },

            SwarmEvent::OutgoingConnectionError { peer_id, error } => {
                if peer_id.is_some() {
                    if let Some(dial_sender) = self.pending_dials.remove(&peer_id.unwrap()) {
                        // TODO: check if its a Boot Node that failed and send a custom error:
                        // `Could not Dial Specified BootNode please check your config file`
                        dial_sender
                            .send(Err(ClientError::JoinRoom(format!(
                                "Couldn't dial Boot Node or Publisher"
                            ))))
                            .expect("to send error back to join_room");
                    }
                }

                debug!(
                    "[OutgoingConnectionError] to peer({:?}), reason {:#}",
                    peer_id, error
                );
            }
            SwarmEvent::ConnectionEstablished {
                peer_id,
                endpoint: libp2p::core::ConnectedPoint::Dialer { address, .. },
                ..
            } => {
                if let Some(dial_sender) = self.pending_dials.remove(&peer_id) {
                    dial_sender
                        .send(Ok(()))
                        .expect("receiver not to be dropped");
                }
                info!(
                    "[ConnectionEstablished] to peer({:?}) on {:?}",
                    peer_id, address
                );
            }
            e => debug!("[OTHER EVENTS] {:?}", e),
        }
    }
    // }}}
}
// }}}

// Composed Behaviour and Event {{{
//  TODO:
//  Auto NAT TRAVERSAL:
//  ----------------------
// Right now a node wether public or private acts as a relay client.
// If User Private with confidence of 3 make us RelayClient and connect to a relay discovered
// through kademlia and somehow advertise my address?
//
// If User is Public advertise as a Relay Node
//
// Maybe use `pub relay: Either<RelayClient, RelayServer>`

// To Advertise relay publish a ProviderRecord of Key::to_bytes("RELAY")
// and use find_closest(RELAY) to get obtain relay nodes

#[derive(NetworkBehaviour)]
#[behaviour(event_process = false, out_event = "ComposedEvent")]
struct ComposedBehaviour {
    kademlia: Kademlia<MemoryStore>,
    gossipsub: Gossipsub,
    identity: Identify,
    auto_nat: autonat::Behaviour,
    dcutr: dcutr::behaviour::Behaviour,
    request_response: RequestResponse<FileExchangeCodec>,
    relay_client: RelayClient::Client,
    ping: Ping,
}

#[derive(Debug)]
pub enum ComposedEvent {
    Kadvent(KademliaEvent),
    Identify(IdentifyEvent),
    AutoNat(autonat::Event),
    Gossipsub(GossipsubEvent),
    Dctur(dcutr::behaviour::Event),
    ReqRes(RequestResponseEvent<FileRequest, FileResponse>),
    RelayClientEvent(RelayClient::Event),
    Ping(PingEvent),
}
impl From<PingEvent> for ComposedEvent {
    fn from(e: PingEvent) -> Self {
        ComposedEvent::Ping(e)
    }
}

impl From<IdentifyEvent> for ComposedEvent {
    fn from(msg: IdentifyEvent) -> ComposedEvent {
        ComposedEvent::Identify(msg)
    }
}
impl From<KademliaEvent> for ComposedEvent {
    fn from(msg: KademliaEvent) -> ComposedEvent {
        ComposedEvent::Kadvent(msg)
    }
}
impl From<GossipsubEvent> for ComposedEvent {
    fn from(msg: GossipsubEvent) -> ComposedEvent {
        ComposedEvent::Gossipsub(msg)
    }
}
impl From<autonat::Event> for ComposedEvent {
    fn from(msg: autonat::Event) -> ComposedEvent {
        ComposedEvent::AutoNat(msg)
    }
}
impl From<dcutr::behaviour::Event> for ComposedEvent {
    fn from(msg: dcutr::behaviour::Event) -> ComposedEvent {
        ComposedEvent::Dctur(msg)
    }
}
impl From<RequestResponseEvent<FileRequest, FileResponse>> for ComposedEvent {
    fn from(event: RequestResponseEvent<FileRequest, FileResponse>) -> Self {
        ComposedEvent::ReqRes(event)
    }
}
impl From<RelayClient::Event> for ComposedEvent {
    fn from(msg: RelayClient::Event) -> ComposedEvent {
        ComposedEvent::RelayClientEvent(msg)
    }
}

// }}}

// FileExchange Logic {{{
#[derive(Debug, Clone)]
pub struct FileExchangeProtocol();
#[derive(Clone)]
pub struct FileExchangeCodec();
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FileRequest(String);
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FileResponse(Vec<u8>);

impl ProtocolName for FileExchangeProtocol {
    fn protocol_name(&self) -> &[u8] {
        "/onef/file-exchange/1".as_bytes()
    }
}

use async_trait::async_trait;

#[async_trait]
impl RequestResponseCodec for FileExchangeCodec {
    type Protocol = FileExchangeProtocol;
    type Request = FileRequest;
    type Response = FileResponse;

    async fn read_request<T>(
        &mut self,
        _: &FileExchangeProtocol,
        io: &mut T,
    ) -> io::Result<Self::Request>
    where
        T: AsyncRead + Unpin + Send,
    {
        let vec = read_length_prefixed(io, 1_000_000).await?;

        if vec.is_empty() {
            return Err(io::ErrorKind::UnexpectedEof.into());
        }

        Ok(FileRequest(String::from_utf8(vec).unwrap()))
    }

    async fn read_response<T>(
        &mut self,
        _: &FileExchangeProtocol,
        io: &mut T,
    ) -> io::Result<Self::Response>
    where
        T: AsyncRead + Unpin + Send,
    {
        let vec = read_length_prefixed(io, 1073741824).await?;

        if vec.is_empty() {
            return Err(io::ErrorKind::UnexpectedEof.into());
        }

        Ok(FileResponse(vec))
    }

    async fn write_request<T>(
        &mut self,
        _: &FileExchangeProtocol,
        io: &mut T,
        FileRequest(data): FileRequest,
    ) -> io::Result<()>
    where
        T: AsyncWrite + Unpin + Send,
    {
        write_length_prefixed(io, data).await?;
        io.close().await?;

        Ok(())
    }

    async fn write_response<T>(
        &mut self,
        _: &FileExchangeProtocol,
        io: &mut T,
        FileResponse(data): FileResponse,
    ) -> io::Result<()>
    where
        T: AsyncWrite + Unpin + Send,
    {
        write_length_prefixed(io, &data).await?;
        Ok(())
    }
}

// }}}

// Client logic {{{
#[derive(Debug)]
pub struct Client {
    pub id: PeerId,
    status: ClientStatus,
    command_sender: mpsc::Sender<Command>,
}

#[derive(Clone, Debug)]
pub struct ChatMessage(pub String, pub String);

#[derive(Clone, Debug)]
pub enum ClientStatus {
    InRoom(room::Room),
    InLobby, //when only connected to the dht
    NotConnected,
}

#[allow(unused_variables, dead_code)]
impl Client {
    // starts a listener with the specified PORT on 0.0.0.0:PORT and bootsrap kad
    pub async fn join_lobby(&mut self, listen_port: u16) -> Result<(), ClientError> {
        let (sender, receiver) = oneshot::channel();
        let addr = format!("/ip4/0.0.0.0/tcp/{}", listen_port)
            .parse()
            .expect("Could not Parse and build the listen multiaddr, check your port number");
        self.command_sender
            .send(Command::JoinLobby { addr, sender })
            .await?;

        receiver.await?
    }

    pub async fn join_room(
        &mut self,
        room_id: String,
    ) -> Result<(Room, mpsc::UnboundedReceiver<ChatMessage>), ClientError> {
        //Find Room and it's publisher
        let (find_sender, find_receiver) = oneshot::channel();

        self.command_sender
            .send(Command::FindRoom {
                room_id: room_id.clone(),
                sender: find_sender,
            })
            .await?;

        let (publisher, room_topic) = find_receiver.await??;
        let mut room = Room::new(room_topic);
        room.id = room_id;

        let (dial_sender, dial_receiver) = oneshot::channel();
        self.command_sender
            .send(Command::DialPublisher {
                publisher,
                sender: dial_sender,
            })
            .await?;

        dial_receiver
            .await
            .map(|e| ClientError::JoinRoom("Failed to Dial Publisher".into()))?;

        let (join_sender, join_receiver) = oneshot::channel();
        self.command_sender
            .send(Command::JoinRoom {
                metadata: (publisher, room.topic.clone()),
                sender: join_sender,
            })
            .await?;

        let messages_receiver = join_receiver.await??;
        println!("went to join");

        self.status = ClientStatus::InRoom(room.clone());
        Ok((room, messages_receiver))
    }

    pub async fn create_room(
        &mut self,
        room_topic: String,
    ) -> Result<(Room, mpsc::UnboundedReceiver<ChatMessage>), ClientError> {
        let room = Room::new(room_topic.clone());
        let (create_sender, create_receiver) = oneshot::channel();
        let (messages_sender, messages_receiver) = mpsc::unbounded::<ChatMessage>();

        self.command_sender
            .send(Command::CreateRoom {
                room: room.clone(),
                sender: create_sender,
            })
            .await?;

        let _ = create_receiver.await??;

        self.command_sender
            .send(Command::SubRoom {
                topic: room.topic.clone(),
                messages_sender,
            })
            .await?;

        self.status = ClientStatus::InRoom(room.clone());
        Ok((room, messages_receiver))
    }

    pub async fn leave_room(&mut self) -> Result<(), ClientError> {
        match &self.status {
            ClientStatus::InRoom(room) => {
                let (leave_sender, leave_receiver) = oneshot::channel();
                self.command_sender
                    .send(Command::LeaveRoom {
                        room: room.clone(),
                        sender: leave_sender,
                    })
                    .await?;

                if let Ok(_) = leave_receiver.await {
                    self.status = ClientStatus::InLobby;
                    Ok(())
                } else {
                    Err(ClientError::LeaveRoom("Could not leave room".into()))
                }
            }
            _ => Err(ClientError::NotInARoom),
        }
    }

    pub async fn send_message(&mut self, msg: String) -> Result<(), ClientError> {
        // Check if a user in a room, publish to the room's topic
        if let ClientStatus::InRoom(room) = &self.status {
            let (sender, receiver) = oneshot::channel();
            self.command_sender
                .send(Command::SendMessage {
                    msg,
                    room_topic: room.topic.clone(),
                    sender,
                })
                .await?;
            let _ = receiver.await?;
            Ok(())
        } else {
            Err(ClientError::NotInARoom)
        }
    }

    pub fn status(&self) -> &ClientStatus {
        &self.status
    }

    pub async fn provide_file(&mut self, fpath: PathBuf, name: String) -> Result<(), ClientError> {
        self.command_sender
            .send(Command::ProvideFile { fpath })
            .await?;
        self.send_message(format!("/nfile/{}/{}", self.id, name))
            .await?;

        Ok(())
    }

    pub async fn request_file(&mut self, peer: PeerId, fname: String) -> Result<(), ClientError> {
        let (sender, receiver) = oneshot::channel();
        self.command_sender
            .send(Command::RequestFile {
                fname,
                peer,
                sender,
            })
            .await?;
        let _ = receiver.await?;
        Ok(())
    }
}
// }}}

// vim:foldmethod=marker
